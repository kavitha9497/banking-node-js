// Import Fastify and banking routes
const fastify = require('./routes/bankingRoutes');

// Define server port
const PORT = process.env.PORT || 3000;

// Start the server
const start = async () => {
    try {
        await fastify.listen(PORT);
        console.log(`Server is running on port ${PORT}`);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
};

start();
